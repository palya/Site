# Palya.fr

Le site [palya.fr](https://palya.fr) est généré à partir des informations contenues dans ce dépôt.

Cette configuration est intéressante car elle repose sur les outils suivants :
- [Jekyll](http://jekyllrb.com/) qui automatise la génération de site statique,
- [Markdown](http://daringfireball.net/projects/markdown/) qui transforme de simples fichiers textes en jolis pages et articles visibles en ligne et enfin
- [GitLab - CI](https://gitlab.com/help/ci/quick_start/README) qui permet de mettre à jour la version en ligne automatiquement.
